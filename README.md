# VHLT

THIS IS OUTDATED. Please visit [github repo](https://github.com/MyLittleRobo/vhlt)

Custom Zoner's Half-Life Tools created by vluzacn. 
This is not my project, I just need repository here to make Linux-compatible version. 
For further information about vhlt see [svencoop forums](http://forums.svencoop.com/showthread.php/38059-ARCHIVE-Custom-ZHLT-by-vluzacn)

## Building on Linux

You need GNU make utility and C++ compiler (g++ or clang++). By default it will build 32-bit binaries. See below how to build in x64 mode. 
To build 32-bit binarues if you have 64-bit Debian-like distro you should download multilib:

    sudo apt-get install gcc-multilib g++-multilib ia32-libs
    
Other distros may provide similiar packages but use different names for them and use other package manager then apt-get. 
The next would probably work for Fedora:

    sudo yum install glibc-devel.i686 libstdc++-devel.i686
    
To build all compilers cd to 'vhlt' directory and type in terminal:

    make

By default g++ is used. To compile with clang++ type:

    make CXX=clang++

You also can build only certain compiler(s) by typing:

    make name1 name2...

Example:

    make hlcsg hlbsp

To delete all object files type:

    make clean
    
Note that executables are not removed by script. To remove executables too type:

    make distclean

To build compilers with support of BSP 31 you should make clean and then make again in this way:

    make USER_DEFINES=-DZHLT_NEW_BSP_VERSION

If you want to try experimental 64-bit versions of compilers type this:

    make ARCH=-m64
    
Don't forget to make clean before (if you already built some other version earlier), since make does not recognize changes in command line options and will assume, that there is nothing to be done.

List of user options:

    ARCH="target architecture"  # specify target platform. By default -m32 is used
    USER_DEFINES="your defines started with -D" # pass additional macro definitions to preprocessor
    USER_FLAGS="your flags"     # flags will be added to CFLAGS and passed to compiler
    INSTALL_PATH=/path/to/dir   # this path (should be given without trailing /) will be used in make install and make uninstall. By default /usr/local/bin is used

To make binaries more portable across Linux distributions you may want to build this way:

    make USER_FLAGS="-static-libgcc -static-libstdc++"

or for x64:

    make ARCH=-m64 USER_FLAGS="-static-libgcc -static-libstdc++"

## Building on Windows with MinGW

When building on Windows with MinGW you should use 'mingw32-make' everywhere instead of just 'make'. For example:

    mingw32-make hlcsg

## Building on FreeBSD

When building on FreeBSD use 'gmake' everywhere instead of 'make'. 
Also you may want to use clang++ as C++ compiler instead of g++, since clang++ is default on FreeBSD.
Example:

    gmake CXX=clang++

## Compiling test map

After you built compilers, you may try to compile test map. First define WADROOT variable.
Bash:

    export WADROOT=$HOME/.steam/steam/SteamApps/common/Half-Life
    
Tsch:
 
    setenv WADROOT "$HOME/.steam/steam/SteamApps/common/Half-Life"

On Windows:

    set WADROOT=C:\Program Files\Steam\SteamApps\common\Half-Life
    
After you defined WADROOT type this (in the same terminal):

    cd bin-m32 # or bin-m64 if you compiled x64 version
    make MAPS=testmap

To delete bsp and other generated files type:

    make MAPS=testmap clean

Note that you may want to specify **-threads** option for every compiler since vhlt does not determine the number of processors/cores on non-windows systems. 
I added support for autodetection, but it still may not properly work. Look at output of compilers.

## Using cppcheck for static code analysis

Assumed you have installed cppcheck utility:

    make check // use cppcheck to check all sources
    make checkcommon // use cppcheck to check source files in 'common' directory
    make checkhlcsg // use cppcheck to check source file of appropriate compiler
    make checkhlbsp // ditto
    make checkhlrad // ditto
    make checkhlvis // ditto

Use CPPCHECKFLAGS variable to alter cppcheck options

Note that currently most of errors are false alarms or insignificant.
    